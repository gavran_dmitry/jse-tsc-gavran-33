package ru.tsc.gavran.tm.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.gavran.tm.api.repository.*;
import ru.tsc.gavran.tm.api.service.*;
import ru.tsc.gavran.tm.command.AbstractCommand;
import ru.tsc.gavran.tm.command.system.ExitCommand;
import ru.tsc.gavran.tm.endpoint.*;
import ru.tsc.gavran.tm.enumerated.Role;
import ru.tsc.gavran.tm.exception.system.AccessDeniedException;
import ru.tsc.gavran.tm.exception.system.UnknownCommandException;
import ru.tsc.gavran.tm.repository.*;
import ru.tsc.gavran.tm.service.*;
import ru.tsc.gavran.tm.util.SystemUtil;
import ru.tsc.gavran.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Getter
@Setter
public class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final LogScanner logScanner = new LogScanner();

    @NotNull
    private final Backup backup = new Backup(this, propertyService);

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this, propertyService);

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();

    @NotNull
    private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();

    @Nullable
    private Session session;

    @NotNull
    private final AdminDataEndpointService adminDataEndpointService = new AdminDataEndpointService();

    @NotNull
    private final AdminDataEndpoint adminDataEndpoint = adminDataEndpointService.getAdminDataEndpointPort();

    @NotNull
    private final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();

    public void start(String[] args) {
        displayWelcome();
//        logScanner.init();
//        initUsers();
//        initData();
        initCommands();
//        initPID();
       parseArgs(args);
        process();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }


    @SneakyThrows
    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.tsc.gavran.tm.command");
        @NotNull final List<Class<? extends AbstractCommand>> classes = reflections
                .getSubTypesOf(ru.tsc.gavran.tm.command.AbstractCommand.class)
                .stream()
                .collect(Collectors.toList());
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            if (Modifier.isAbstract(clazz.getModifiers())) continue;
            try {
                registry(clazz.newInstance());
            } catch (@NotNull final Exception e) {
                logService.error(e);
            }
        }
    }

//    public void initUsers() {
//
//        userService.create("Test", "Test", Role.USER);
//        userService.updateUserByLogin("Test", "LTest1", "FTest", "MTest", "test@gmail.com");
//        userService.create("Admin", "Admin", Role.ADMIN);
//        userService.updateUserByLogin("Admin", "GAVRAN", "Dmitry", "Mihailovich", "gavran@gmail.com");
//    }
//
//    private void initData() {
//        projectService.create(userRepository.findByLogin("Test").getId(), "Project C", "1");
//        projectService.create(userRepository.findByLogin("Test").getId(), "Project A", "2");
//        projectService.create(userRepository.findByLogin("Admin").getId(), "Project B", "3");
//        projectService.create(userRepository.findByLogin("Admin").getId(), "Project D", "4");
//        taskService.create(userRepository.findByLogin("Test").getId(), "Task C", "1");
//        taskService.create(userRepository.findByLogin("Test").getId(), "Task A", "2");
//        taskService.create(userRepository.findByLogin("Admin").getId(), "Task B", "3");
//        taskService.create(userRepository.findByLogin("Admin").getId(), "Task D", "4");
//        projectService.finishByName(userRepository.findByLogin("Test").getId(), "Project C");
//        projectService.startByName(userRepository.findByLogin("Admin").getId(), "Project D");
//        taskService.finishByName(userRepository.findByLogin("Test").getId(), "Task C");
//        taskService.startByName(userRepository.findByLogin("Admin").getId(), "Task B");
//    }

    private void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **"+"\n");
    }

    private void commandCompleted(@Nullable final String command) {
        if (System.console() == null) {
            System.out.println("\n" + TerminalUtil.ANSI_GREEN + "Command '" + command + "' completed." + TerminalUtil.ANSI_RESET + "\n");
        }
        else {
            System.out.println("\n" + "Command '" + command + "' completed." + "\n");
        }

    }

    private void displayAuth() {
        if (System.console() == null) {
            System.out.println(TerminalUtil.ANSI_RED + TerminalUtil.AUTHORIZED + TerminalUtil.ANSI_RESET);
        }
        else {
            System.out.println(TerminalUtil.AUTHORIZED);
        }
        System.out.print("\n");
        System.out.println(commandService.getCommandByName("login"));
        System.out.println(commandService.getCommandByName("exit"));
        System.out.println(commandService.getCommandByName("user-create"));
        System.out.println(commandService.getCommandByName("about"));
        System.out.println(commandService.getCommandByName("version"));
        System.out.print("\n");
    }

    private void process() {
//        logService.debug("Log write started.");
        String exit = new ExitCommand().name();
        @NotNull
        String command = "";
//        backup.init();
//        fileScanner.init();
        while (!exit.equals(command)) {
            try {
                while (getSession() == null) { //работа с неаутентифицированными пользователями
                    try {
                        displayAuth();
                        command = TerminalUtil.nextLine();
                        switch (command) {
                            case "exit":
                                parseCommand("exit");
                                break;
                            case "login":
                                parseCommand("login");
                                break;
                            case "user-create":
                                parseCommand("user-create");
                                break;
                            case "about":
                                parseCommand("about");
                                break;
                            case "version":
                                parseCommand("version");
                                break;
                            case "":
                                break;
                            default:
                                throw new AccessDeniedException();
                        }
                    } catch (@NotNull final Exception e) {
                        logService.error(e);
                    }
                }
                System.out.println("ENTER COMMAND:");
                command = TerminalUtil.nextLine();
                logService.debug(command);
                parseCommand(command);
                logService.debug("Completed.");
                commandCompleted(command);
            } catch (@NotNull final Exception e) {
                logService.error(e);
            }
        }
        backup.stop();
        fileScanner.stop();
    }

    public void parseArg(@Nullable final String arg) {
        @Nullable
        AbstractCommand arguments = commandService.getCommandByArg(arg);
        if (!Optional.ofNullable(arguments).isPresent()) throw new UnknownCommandException();
        arguments.execute();
    }

    public void parseCommand(@Nullable final String command) {
        if (!Optional.ofNullable(command).isPresent() || command.isEmpty()) return;
        @Nullable
        AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (!Optional.ofNullable(abstractCommand).isPresent()) throw new UnknownCommandException();
        abstractCommand.execute();
        if (Thread.currentThread().getName().equals("ScannerThread")) {
            commandCompleted(command);
            System.out.println("ENTER COMMAND:");
        }
    }

    private void registry(@Nullable AbstractCommand command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void parseArgs(@Nullable String[] args) {
        if (!Optional.ofNullable(args).isPresent() || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

}