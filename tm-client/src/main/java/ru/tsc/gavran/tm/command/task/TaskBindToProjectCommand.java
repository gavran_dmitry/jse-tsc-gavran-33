package ru.tsc.gavran.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.command.AbstractTaskCommand;
import ru.tsc.gavran.tm.endpoint.Project;
import ru.tsc.gavran.tm.endpoint.Role;
import ru.tsc.gavran.tm.endpoint.Session;
import ru.tsc.gavran.tm.endpoint.Task;
import ru.tsc.gavran.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.gavran.tm.exception.entity.TaskNotFoundException;
import ru.tsc.gavran.tm.exception.system.AccessDeniedException;
import ru.tsc.gavran.tm.exception.system.ProcessException;
import ru.tsc.gavran.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskBindToProjectCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-bind-to-project";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Bind task to project.";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        final String userId = session.getUserId();
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @Nullable final Project project = serviceLocator.getProjectEndpoint().findProjectById(session, projectId);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ENTER TASK ID:");
        @Nullable final String taskId = TerminalUtil.nextLine();
        @Nullable final Task task = serviceLocator.getTaskEndpoint().findTaskById(session, taskId);
        if (task == null) throw new TaskNotFoundException();
        @Nullable Task taskToProject = serviceLocator.getTaskEndpoint().bindTaskById(session, projectId, taskId);
        if (taskToProject == null) throw new ProcessException();
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}