package ru.tsc.gavran.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getApplicationVersion();

    @NotNull
    Integer getBackupInterval();

    @NotNull
    Integer getFileScannerInterval();

}