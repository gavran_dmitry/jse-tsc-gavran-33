package ru.tsc.gavran.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.command.AbstractProjectCommand;
import ru.tsc.gavran.tm.endpoint.Project;
import ru.tsc.gavran.tm.endpoint.Role;
import ru.tsc.gavran.tm.endpoint.Session;
import ru.tsc.gavran.tm.enumerated.Sort;
import ru.tsc.gavran.tm.exception.system.AccessDeniedException;
import ru.tsc.gavran.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class ProjectShowListCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show a list of projects.";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        final String userId = session.getUserId();
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sort = TerminalUtil.nextLine();
        @NotNull List<Project> projects;
        if (sort == null || sort.isEmpty()) projects = serviceLocator.getProjectEndpoint().findProjectAll(session);
        else {
            Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            projects = serviceLocator.getProjectEndpoint().findProjectAll(session);
        }
        int index = 1;
        for (Project project : projects) {
            System.out.println(index + ". " + project.toString());
            index++;
        }
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}