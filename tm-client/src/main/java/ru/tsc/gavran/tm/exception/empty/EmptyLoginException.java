package ru.tsc.gavran.tm.exception.empty;

import ru.tsc.gavran.tm.exception.AbstractException;

public class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error! Login is empty.");
    }

}
