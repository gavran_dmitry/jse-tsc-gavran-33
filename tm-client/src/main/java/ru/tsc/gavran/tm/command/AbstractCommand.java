package ru.tsc.gavran.tm.command;

import ru.tsc.gavran.tm.api.service.IServiceLocator;
import ru.tsc.gavran.tm.endpoint.Role;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public Role[] roles() {
        return null;
    }

    public abstract String name();

    public abstract String arg();

    public abstract String description();

    public abstract void execute();

    @Override
    public String toString() {
        String result = "";
        String name = name();
        String arg = arg();
        String description = description();

        if (name != null && !name.isEmpty()) result += name;
        if (arg != null && !arg.isEmpty()) result += " [" + arg + "]";
        if (description != null && !description.isEmpty()) result += " - " + description;
        return result;

    }

}
