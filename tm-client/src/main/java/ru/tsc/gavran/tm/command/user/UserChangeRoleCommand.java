package ru.tsc.gavran.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.command.AbstractUserCommand;
import ru.tsc.gavran.tm.endpoint.Role;
import ru.tsc.gavran.tm.endpoint.Session;
import ru.tsc.gavran.tm.endpoint.User;
import ru.tsc.gavran.tm.exception.entity.UserNotFoundException;
import ru.tsc.gavran.tm.exception.system.AccessDeniedException;
import ru.tsc.gavran.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

public class UserChangeRoleCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String name() {
        return "user-change-role";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Change the user role.";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        System.out.println("ENTER USER ID: ");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final User user = serviceLocator.getAdminUserEndpoint().findUserById(session, id);
        if (user == null) throw new UserNotFoundException();
        System.out.println("ENTER NEW ROLE: ");
        System.out.println(Arrays.toString(Role.values()));
        @Nullable final String roleValue = TerminalUtil.nextLine();
        @NotNull final Role role = Role.valueOf(roleValue);
        serviceLocator.getAdminUserEndpoint().setUserRole(session, role);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}