package ru.tsc.gavran.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.endpoint.Role;
import ru.tsc.gavran.tm.endpoint.Session;
import ru.tsc.gavran.tm.exception.system.AccessDeniedException;

import java.util.Optional;

public class DataJsonSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String name() {
        return "data-json-save";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to json file (Faster XML).";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        serviceLocator.getAdminDataEndpoint().saveDataJson(session);
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
