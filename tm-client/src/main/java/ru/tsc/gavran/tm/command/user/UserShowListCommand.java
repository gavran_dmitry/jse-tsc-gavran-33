package ru.tsc.gavran.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.command.AbstractUserCommand;
import ru.tsc.gavran.tm.endpoint.Role;
import ru.tsc.gavran.tm.endpoint.Session;
import ru.tsc.gavran.tm.endpoint.User;
import ru.tsc.gavran.tm.exception.system.AccessDeniedException;
import ru.tsc.gavran.tm.exception.system.ProcessException;

import java.util.List;
import java.util.Optional;

public class UserShowListCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String name() {
        return "user-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show all users.";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        System.out.println("List of users.");
        @Nullable final List<User> users = serviceLocator.getAdminUserEndpoint().findAllUser(session);
        if (users == null) throw new ProcessException();
        for (final User user : users) showUser(user);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}