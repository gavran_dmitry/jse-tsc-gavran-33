package ru.tsc.gavran.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.command.AbstractTaskCommand;
import ru.tsc.gavran.tm.endpoint.Role;
import ru.tsc.gavran.tm.endpoint.Session;
import ru.tsc.gavran.tm.endpoint.Task;
import ru.tsc.gavran.tm.exception.entity.TaskNotFoundException;
import ru.tsc.gavran.tm.exception.system.AccessDeniedException;
import ru.tsc.gavran.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by index.";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final Task task = serviceLocator.getTaskEndpoint().findTaskByIndex(session, index);
        if (task == null) throw new TaskNotFoundException();
        serviceLocator.getTaskEndpoint().removeTaskByIndex(session, index);
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}