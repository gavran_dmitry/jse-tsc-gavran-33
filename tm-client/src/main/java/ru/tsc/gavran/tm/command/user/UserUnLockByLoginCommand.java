package ru.tsc.gavran.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.command.AbstractUserCommand;
import ru.tsc.gavran.tm.endpoint.Role;
import ru.tsc.gavran.tm.endpoint.Session;
import ru.tsc.gavran.tm.exception.system.AccessDeniedException;
import ru.tsc.gavran.tm.util.TerminalUtil;

import java.util.Optional;

public class UserUnLockByLoginCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String name() {
        return "user-unlock-by-login";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Unlock user login.";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        System.out.println("ENTER USER LOGIN: ");
        @Nullable final String login = TerminalUtil.nextLine();
        @Nullable final String id = serviceLocator.getAdminUserEndpoint().findUserByLogin(session, login).getId();
        @NotNull final String currentUserId = serviceLocator.getSession().getUserId();
        if (id.equals(currentUserId)) throw new AccessDeniedException();
        serviceLocator.getAdminUserEndpoint().unlockUserByLogin(session, login);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}