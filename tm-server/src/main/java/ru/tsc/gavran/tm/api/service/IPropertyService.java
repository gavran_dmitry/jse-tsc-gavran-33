package ru.tsc.gavran.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService extends ISaltSettings {

    @NotNull
    String getApplicationVersion();

    @NotNull
    Integer getBackupInterval();

    @NotNull
    Integer getFileScannerInterval();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getServerHost();

}