package ru.tsc.gavran.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.repository.IOwnerRepository;
import ru.tsc.gavran.tm.api.repository.ISessionRepository;
import ru.tsc.gavran.tm.api.service.IServiceLocator;
import ru.tsc.gavran.tm.api.service.ISessionService;
import ru.tsc.gavran.tm.enumerated.Role;
import ru.tsc.gavran.tm.exception.entity.UserNotFoundException;
import ru.tsc.gavran.tm.exception.system.AccessDeniedException;
import ru.tsc.gavran.tm.model.Session;
import ru.tsc.gavran.tm.model.User;
import ru.tsc.gavran.tm.util.HashUtil;

public class SessionService extends AbstractOwnerService<Session> implements ISessionService {

    @NotNull
    private final ISessionRepository sessionRepository;

    @NotNull
    private final IServiceLocator serviceLocator;

    public SessionService(
            @NotNull final ISessionRepository sessionRepository,
            @NotNull final IServiceLocator serviceLocator
    ) {
        super(sessionRepository);
        this.sessionRepository = sessionRepository;
        this.serviceLocator = serviceLocator;
    }

    @Override
    public boolean checkDataAccess(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        @Nullable final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return false;
        if (user.getLocked()) return false;
        @Nullable final String hash = HashUtil.salt(serviceLocator.getPropertyService(), password);
        if (hash == null) return false;
        return hash.equals(user.getPasswordHash());
    }

    @Override
    public void close(@NotNull Session session) {
        validate(session);
        remove(session);
    }

    @Override
    public Session open(@Nullable String login, @Nullable String password) {
        boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        @Nullable final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        sessionRepository.add(session);
        return session;
    }

    @Override
    public void validate(@NotNull Session session, @Nullable Role role) throws AccessDeniedException{
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new AccessDeniedException();
        if (user.getRole() == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    public void validate(@Nullable Session session) throws AccessDeniedException {
        if (session == null) throw new AccessDeniedException();
        if (session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        if (!sessionRepository.contains(session.getId())) throw new AccessDeniedException();
    }

    @Nullable
    @Override
    public User getUser(@NotNull final Session session) {
        validate(session);
        @NotNull final String userId = session.getUserId();
        return serviceLocator.getUserService().findById(userId);
    }
}
