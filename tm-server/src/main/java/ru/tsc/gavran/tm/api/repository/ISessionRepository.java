package ru.tsc.gavran.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gavran.tm.model.Session;

public interface ISessionRepository extends IOwnerRepository<Session> {

    boolean contains (@NotNull String id);

}
