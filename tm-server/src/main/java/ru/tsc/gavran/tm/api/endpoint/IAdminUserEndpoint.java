package ru.tsc.gavran.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.enumerated.Role;
import ru.tsc.gavran.tm.model.Session;
import ru.tsc.gavran.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IAdminUserEndpoint {

    @WebMethod
    User lockUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "login", partName = "login") @NotNull String login
    );

    @WebMethod
    User removeUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "login", partName = "login") @NotNull String login
    );

    @WebMethod
    User removeUserById(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @WebMethod
    User unlockUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "login", partName = "login") @NotNull String login
    );

    User setUserRole(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "role", partName = "role") @NotNull Role role
    );

    @WebMethod
    User updateUserById(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "firstName", partName = "firstName") @NotNull String firstName,
            @WebParam(name = "lastName", partName = "lastName") @NotNull String lastName,
            @WebParam(name = "middleName", partName = "middleName") @NotNull String middleName,
            @WebParam(name = "email", partName = "email") @NotNull String email
    );

    @WebMethod
    User updateUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "login", partName = "login") @NotNull String login,
            @WebParam(name = "firstName", partName = "firstName") @NotNull String firstName,
            @WebParam(name = "lastName", partName = "lastName") @NotNull String lastName,
            @WebParam(name = "middleName", partName = "middleName") @NotNull String middleName,
            @WebParam(name = "email", partName = "email") @NotNull String email
    );

    @WebMethod
    void updateUserPassword(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "password", partName = "password") @NotNull String password
    );

    @Nullable
    @WebMethod
    User getCurrentUser(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    );

    @Nullable
    @WebMethod
    User findUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "login", partName = "login") @NotNull String login
    );

    @Nullable
    @WebMethod
    User findUserById(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "idn", partName = "id") @NotNull String id
    );

    @Nullable
    @WebMethod
    void clearUser(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    );

    @Nullable
    @WebMethod
    List<User> findAllUser(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    );

}