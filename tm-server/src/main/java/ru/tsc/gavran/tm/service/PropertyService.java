package ru.tsc.gavran.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "application.version";

    @NotNull
    private static final String APPLICATION_VERSION_DEFAULT = "";

    @NotNull
    private static final String BACKUP_INTERVAL_DEFAULT = "30";

    @NotNull
    private static final String BACKUP_INTERVAL_KEY = "backup.interval";

    @NotNull
    private static final String FILE_SCANNER_INTERVAL_DEFAULT = "10";

    @NotNull
    private static final String FILE_SCANNER_INTERVAL_KEY = "scanner.interval";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "8080";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SERVER_HOST_DEFAULT = "localhost";

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    private String getValue(String name, String defaultValue) {
        @Nullable final String systemProperty = System.getProperty(name);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getenv(name);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(name, defaultValue);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        @Nullable final String systemProperty = System.getProperty(PASSWORD_ITERATION_KEY);
        if (systemProperty != null) return Integer.parseInt(systemProperty);
        @Nullable final String environmentProperty = System.getenv(PASSWORD_ITERATION_KEY);
        if (environmentProperty != null) return Integer.parseInt(environmentProperty);
        return Integer.parseInt(properties.getProperty(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT));
    }

    private int getValueInt(final String name, final String defaultValue) {
        @Nullable
        final String systemProperty = System.getProperty(name);
        if (systemProperty != null) return Integer.parseInt(systemProperty);
        @Nullable
        final String environmentProperty = System.getenv(name);
        if (environmentProperty != null) return Integer.parseInt(environmentProperty);
        return Integer.parseInt(properties.getProperty(name, defaultValue));
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return getValue(APPLICATION_VERSION_KEY, APPLICATION_VERSION_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getBackupInterval() {
        return getValueInt(BACKUP_INTERVAL_KEY, BACKUP_INTERVAL_DEFAULT);
    }

    @Override
    public @NotNull Integer getFileScannerInterval() {
        return getValueInt(FILE_SCANNER_INTERVAL_KEY, FILE_SCANNER_INTERVAL_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        return getValueInt(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    }

    @NotNull
    @Override
    public String getServerHost() {
        return getValue(SERVER_HOST_KEY, SERVER_HOST_KEY);
    }

}